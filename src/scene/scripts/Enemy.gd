extends KinematicBody2D

export (PackedScene) var Bullet
var speed = 100
var motion = Vector2.ZERO
var player 

onready var gun = $GunPivot

func _ready():
	player = get_tree().get_nodes_in_group("player")[0]
	
	pass # Replace with function body.

func _physics_process(delta):
	motion = Vector2.ZERO
	if player:
		motion = global_position.direction_to(player.global_position) * speed
		look_at(player.global_position)
	motion = move_and_slide(motion)


func shoot():
	print("hello")
	var bullet = Bullet.instance()
	bullet.owner_group = "enemy"
	get_parent().add_child(bullet)
	bullet.global_transform = gun.global_transform

func _on_Area2D_area_entered(area):
	if area.is_in_group("bullet") and area.owner_group != "enemy":
		queue_free()

func _on_FireRate_timeout():
	shoot()
