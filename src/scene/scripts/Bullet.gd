extends Area2D

export var speed = 200
export var time = 2.0
var owner_group = "enemy"


onready var timer = $Timer

func _ready():
	timer.wait_time = time
	timer.start()

func _process(delta):
	position += transform.x * speed *delta
	
 
func _on_Timer_timeout():
	queue_free()


func _on_Bullet_area_entered(area):
	if area.is_in_group(owner_group):
		return
	queue_free()


func _on_Bullet_body_entered(body):
	if body.is_in_group(owner_group):
		return
	queue_free()
