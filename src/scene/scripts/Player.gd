extends KinematicBody2D


export (PackedScene) var Bullet
export var speed = 200

var hp = 3

var motion = Vector2.ZERO
onready var gun = $GunPivot
onready var label = $Label

func _physics_process(delta):
	label.text = str(hp)
	handle_inputs()
	motion = move_and_slide(motion)
	look_at(get_global_mouse_position())

func handle_inputs():
	motion = Vector2.ZERO
	if Input.is_action_pressed("move_up"):
		motion.y -= 1
	if Input.is_action_pressed("move_down"):
		motion.y += 1
	if Input.is_action_pressed("move_left"):
		motion.x -= 1
	if Input.is_action_pressed("move_right"):
		motion.x += 1
		
	if Input.is_action_just_pressed("fire"):
		shoot()
		
	motion = motion.normalized() * speed

func die():
	get_tree().reload_current_scene()


func shoot():
	var bullet = Bullet.instance()
	bullet.owner_group = "player"
	get_parent().add_child(bullet)
	bullet.transform = gun.global_transform
	


func _on_Area2D_area_entered(area):
	if area.is_in_group("crate"):
		if hp < 3:
			hp+=1
			
	if area.is_in_group("bullet"):
		hp -= 1
		if hp < 1:
			die()
