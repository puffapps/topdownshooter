extends Node2D

export (PackedScene) var Item

var delay = 1.0

onready var timer = $Timer


func _ready():
	timer.wait_time = delay
	timer.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	randomize()
	delay = rand_range(1,3)
	var item = Item.instance()
	add_child(item)
	timer.wait_time = delay
